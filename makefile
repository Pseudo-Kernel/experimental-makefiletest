

CC = gcc
CFLAGS = -masm=intel -save-temps

# GetFileList (filter)
GetFileList = $(shell powershell "Resolve-Path -Path (Get-ChildItem $1 -Force -Recurse) -Relative")

SRCS := $(call GetFileList,*.c)
DEPS := $(SRCS:.c=.d)
OBJS := $(SRCS:.c=.o)
PREPS := $(SRCS:.c=.i)
ASMS := $(SRCS:.c=.s)

.PHONY : clean



clean:
	$(shell cmd /C "del $(DEPS) $(OBJS) $(PREPS) $(ASMS)")

# Currently not working
foo: $(DEPS) $(SRCS)
	$(CC) $(OBJS) -o foo.exe
	@echo $(SRCS) $(OBJS)

# DO NOT put $(DEPS) : $(SRCS) here because %.d: %.c does all work.

%.d: %.c
	$(CC) -MD -MP $<
	@echo DEP_CREATE: $(CC) -M $(CFLAGS) $< -o $@

# Not executed because we're including *.d
%.o: %.c
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@
	@echo OBJ_CREATE: $(CC) -c $(CFLAGS) $(CPPFLAGS) $< -o $@

	
-include $(DEPS)

